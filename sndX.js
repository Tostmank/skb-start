var url = require('url');
var qs = require("querystring");
var express = require('express');
const app = express();
var server = app.listen(3000,function() {} );
var BigNumber = require('Big-Number');

var cors = require('cors');
app.use(cors());

function ph(i)
{
    if (i==0)
        return 1;
    if (i==1)
        return 18;
    if (i==2)
        return 243;
    if (i>2)
    {   
        return BigNumber(ph(i-1)).multiply(12).plus(BigNumber(ph(i-2)).multiply(18)).toString();
    }
    return 0;
}

function letsgo(letter) {
return ph(letter).toString();
};

app.get('/', function(req,res){
var query = url.parse(req.url).query;
var params = qs.parse(query);
var letter = params.i;
console.log(letter.toString());
res.send(letsgo(letter));
});
