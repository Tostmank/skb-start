var url = require('url');
var qs = require("querystring");
var express = require('express');
const app = express();
var server = app.listen(3000,function() {} );

var cors = require('cors');

app.use(cors());

var filterInt = function (value) {
  if(/^(\-|\+)?([0-9]+|Infinity)$/.test(value))
    return Number(value);
  return 0;
};

app.get('/', function(req,res){
var query = url.parse(req.url).query;
params = qs.parse(query);
a = filterInt(params.a);
b = filterInt(params.b);
console.log(a+' '+b);
res.send((a+b).toString()); 
});

