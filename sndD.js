var url = require('url');
var qs = require("querystring");
var express = require('express');
const app = express();
var server = app.listen(3000,function() {} );

var cors = require('cors');
app.use(cors());

function isHex(h) {
hexs = /[0-9A-Fa-f]{6}/g;
hext = /[0-9A-Fa-f]{3}/g;
return (h.match(hexs) && h.match(hext));
}

function componentToHex(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

function duplicateChar(str, pos)
{
    return str.substring(0,pos+1)+str[pos]+str.substring(pos+1,str.length);
}

function hueToRgb(p, q, t){
if(t < 0)
    t += 1;
if(t > 1) 
    t -= 1;
if(t < 1/6) 
    return p + (q - p) * 6 * t;
if(t < 1/2) 
    return q;
if(t < 2/3) 
    return p + (q - p) * (2/3 - t) * 6;
    return p;
}

function rgbToHex(str) {
    var reg = /(\s*.*?)rgb\(\s*(\d+)\s*,\s*(\d+)\s*, \s*(\d+)\s*\)/;
    if (!reg.test(str))
    return 'Invalid color';
    var digits = reg.exec(str);    
    var red = parseInt(digits[2]);
    var green = parseInt(digits[3]);
    var blue = parseInt(digits[4]);
    if (red>255 || red<0 || 
    green>255 || green<0 ||
    blue>255 || blue<0)
    return 'Invalid color';
    
    var rgb = blue | (green << 8) | (red << 16);
    rgb = rgb.toString(16);
    while(rgb.length<6)
    rgb='0'+rgb;
    return rgb;
}

function hslToHex(str) {
    var reg = /(\s*.*?)hsl(a?)\(\s*(\d+\s*%?)\s*,\s*(\d+\s*%)\s*,\s*(\d+\s*%)\s*\)/;//\s*(?:,\s*(\d.+)\s*\))/;
    if (!reg.test(str))
    return 'Invalid color';
    var digits = reg.exec(str);
    hsl = {
    h : digits[3] / 360,
    s : parseFloat(digits[4]) / 100,
    l : parseFloat(digits[5]) / 100,
	a : parseFloat(digits[6])
    };

    if (hsl.s>1 || hsl.l>1)
    return 'Invalid color';

    var red,green,blue;
    
if (hsl.s === 0) {
		let v = 255 * hsl.l;
red = blue = green = v;
	} else {
		let q = hsl.l < 0.5 ? hsl.l * ( 1 + hsl.s ) : ( hsl.l + hsl.s ) - ( hsl.l * hsl.s );
		let p = 2 * hsl.l - q;
		red = Math.floor(255*hueToRgb(p, q, hsl.h + ( 1 / 3 ) ));
		green = Math.round(255*hueToRgb(p, q, hsl.h));
		blue = Math.ceil(255*hueToRgb(p, q, hsl.h - ( 1 / 3 ) ));
		//rgba.a = hsl.a;
	}    
    

    var rgb = blue | (green << 8) | (red << 16);
    rgb = rgb.toString(16);
    while(rgb.length<6)
    rgb='0'+rgb;
    return rgb;

}


function letsgo(url) {
if (url)
    url = url.trim()
else
    return 'Invalid color';
    
while(url[0]==' ')
    url = url.substr(1,url.length+1);
if (url.indexOf('hsl')!=-1)
{
    
    if (url.indexOf('#')!=-1)
        url = 'Invalid color'
    else
        url = hslToHex(url);
}
url = decodeURI(url);
if (url.indexOf("rgb")!=-1)
{
    if (url.indexOf('#')!=-1)
        url = 'Invalid color'
    else
        url = rgbToHex(url);
}
if (url == 'Invalid color')
    return url;

url = url.toLowerCase();
url = url.replace('#','');

if (url.length!=6)
{
    if (url.length==3)
    {
        url=duplicateChar(url,2);
        url=duplicateChar(url,1);
        url=duplicateChar(url,0);
    }
    else
    return 'Invalid color';
}

if (!isHex(url))
return 'Invalid color';
url = '#'+url;
return url;
};

app.get('/', function(req,res){
var query = url.parse(req.url).query;
var params = qs.parse(query);
var color = params.color;
console.log(color);
res.send(letsgo(color));
});
