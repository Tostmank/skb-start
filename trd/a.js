var express = require('express');
var cors = require('cors');
const app = express();
app.use(cors());
const router = express.Router();
var async = require('async');
var await = require('await');
require('es6-promise').polyfill();
require('isomorphic-fetch');
require('es6-promise').polyfill();
require('isomorphic-fetch');

async function getPC() {
	const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

	let pc = {};

	await fetch(pcUrl)
		.then(async (res) => {
			pc = await res.json();
		})
		.catch(err => {
			console.log(err.message, err);
		})

	return pc;
}

getPC();

router.param('resource', async function(req, res, next, resource) {
	let pc = await getPC();

	const data = pc[req.params.resource];
	if(typeof data === "undefined") {
		return next(new Error("Not Found"));
	}

	req.data = data;
	next();
});

router.param('keys', async function(req, res, next, key) {
	let pc = await getPC();
	if (req.params.keys=='length')
		return next(new Error("Not Found"));

	const data = pc[req.params.resource][req.params.keys];
	if(typeof data === "undefined") {
		return next(new Error("Not Found"));
	}

	req.data = data;
	next();
});

router.param('key', async function(req, res, next, key) {
	let pc = await getPC();

	if (req.params.key=='length')
		return next(new Error("Not Found"));

	const data = pc[req.params.resource][req.params.keys][req.params.key];
	if(typeof data === "undefined") {
		return next(new Error("Not Found"));
	}

	req.data = data;
	next();
});

router.param('last', async function(req, res, next, last) {
	let pc = await getPC();

	const data = pc[req.params.resource][req.params.keys][req.params.key][req.params.last];
	if(typeof data === "undefined") {
		return next(new Error("Not Found"));
	}

	req.data = data;
	next();
});

router.get('/',  async function(req, res) {

	let pc = await getPC();

	res.json(pc);
});

router.get('/volumes',  async function(req, res) {

	let pc = await getPC();
	let result = {};

	pc.hdd.forEach(disc => {
			if(!(disc.volume in result)) {
				result[disc.volume] = disc.size + 'B';
			} else {
				result[disc.volume] = (disc.size + parseInt(result[disc.volume])) + 'B';
			}

	});

	res.json(result);

});

router.get('/:resource/', function(req, res) {
	res.json(req.data);
});
router.get('/:resource/:keys', async function(req, res) {
	res.json(req.data);
});

router.get('/:resource/:keys/:key',  async function(req, res) {
	res.json(req.data);
});

router.get('/:resource/:keys/:key/:last',  async function(req, res) {
	res.json(req.data);
});


router.use(function (err, req, res, next) {
  console.error(err.stack);
	res.status(404).send(err.message);
});

app.use('/', router);
var server = app.listen(3000,function() {} );