var express = require('express');
var cors = require('cors');
const app = express();
app.use(cors());
const router = express.Router();
var async = require('async');
var await = require('await');
require('es6-promise').polyfill();
require('isomorphic-fetch');
require('es6-promise').polyfill();
require('isomorphic-fetch');

async function getPets() {
	const petsUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

	let pets = {};

	await fetch(petsUrl)
		.then(async (res) => {
			pets = await res.json();
		})
		.catch(err => {
			console.log(err.message, err);
		})
	return pets;
}

getPets();

function populatePet(pc,pet)
{
	pet.user = pc['users'][pet.userId-1];
}

function populateUser(pc,user)
{
	user.pets = getHisPets(pc,user);
}

function getByType(arr,typer)
{
	return arr.filter(function(pet) {
  return pet.type == typer;
});
}

function sortById(arr)
{
	for (i = 0;i<arr.length-1;i++)
	{
		if (arr[i].id>arr[i+1].id)
		{
			let temp = arr[i];
			arr[i] = arr[i+1];
			arr[i+1] = temp;
			i-=2;
			if (i<0) i = 0;
		}
	}
	return arr;
}

function getByNick(users,nick)
{
	let poosoo = [];
	for (let user of users)
	{
		if (user.username == nick)
			return user;		
	}	
	return (poosoo);
}

function getHisPets(pc,user)
{
	pets = pc['pets'];
	for (let pet of pets)
	{
		return pets.filter(function(pet) {
			return pet.userId == user});
	}	
}


function getByPetType(users,pets)
{
	let poosoo = [];
	for (let pet of pets)
	{
		if (poosoo.indexOf(users[pet.userId-1])<0)
			poosoo.push(users[pet.userId-1]);		
	}	
	return sortById(poosoo);
}


function getByAge(arr,checker,booler)
{
	var k = 0;
	for (let pet of arr)
	{
		if (booler)
		{
			return arr.filter(function(pet) {
  return pet.age >checker});
		}
		else
		{
			return arr.filter(function(pet) {
				return pet.age < checker});
		}
	}	
}



function check(toCheck)
{
	return (typeof toCheck === "undefined");
}

router.param('resource', async function(req, res, next, resource) {
	let pc = await getPets();
	let data = pc[req.params.resource];
	if (resource == 'pets')
	{
		type = req.query.type;
		age_gt = req.query.age_gt;
		age_lt = req.query.age_lt;
		if (type)
			data = getByType(data,type);
		if (age_gt)
			data = getByAge(data,age_gt,true);
		if (age_lt)
			data = getByAge(data,age_lt,false);	
	}
	if (resource == 'users')
	{
		petType = req.query.havePet;
		if (petType)
		{			
			matchingPets = getByType(pc['pets'],petType);				
			data = getByPetType(data,matchingPets);
		}
	}
	if (check(data))
		data = [];
	req.data = data;
	next();
});

router.param('keys', async function(req, res, next, key) {
	let pc = await getPets();
	let data = pc[req.params.resource];
	let pets = pc['pets'];
	let users = pc['users'];
	if (req.params.resource == 'users')
	{
		if (key.match(/^[-\+]?\d+/) != null)
			data = pc[req.params.resource][req.params.keys-1];
		else
		{
			if (key!='populate')
				data = getByNick(data,key);
			else
			{
				data.forEach(function(user)
				{
					populateUser(pc,user);
				});
			}
		}
	}
	if (req.params.resource == 'pets')
	{
		if (key.match(/^[-\+]?\d+/) != null)
			data = pc[req.params.resource][req.params.keys-1];
		else
		{
		data.forEach(function(pet)
			{
				populatePet(pc,pet);
			});
			type = req.query.type;
		age_gt = req.query.age_gt;
		age_lt = req.query.age_lt;
		if (type)
			data = getByType(data,type);
		if (age_gt)
			data = getByAge(data,age_gt,true);
		if (age_lt)
			data = getByAge(data,age_lt,false);	
		}
	}
	if (check(data))
		return next(new Error("Not Found"));

	req.data = data;
	next();
});

router.param('key', async function(req, res, next, key) {
	let pc = await getPets();
	let data = pc[req.params.resource];
	if (req.params.resource == 'users')
	{
	if (key.match(/^[-\+]?\d+/) != null)
		data = pc[req.params.resource][req.params.keys-1];
	else
		data = getHisPets(pc,getByNick(data,key));
	}
	if (req.params.resource == 'pets')
	{
		data = pc[req.params.resource][req.params.keys-1];
		if (key == 'populate')
		populatePet(pc,pc[req.params.resource][req.params.keys-1]);
	}

	if (check(data))
		return next(new Error("Not Found"));

	req.data = data;
	next();
});

router.param('last', async function(req, res, next, last) {
	let pc = await getPets();

	const data = pc[req.params.resource][req.params.keys][req.params.key][req.params.last];
	if (check(data))
		return next(new Error("Not Found"));

	req.data = data;
	next();
});

router.get('/',  async function(req, res) {

	let pc = await getPets();

	res.json(pc);
});

router.get('/volumes',  async function(req, res) {

	let pc = await getPets();
	let result = {};

	pc.hdd.forEach(disc => {
			if(!(disc.volume in result)) {
				result[disc.volume] = disc.size + 'B';
			} else {
				result[disc.volume] = (disc.size + parseInt(result[disc.volume])) + 'B';
			}

	});

	res.json(result);

});

router.get('/:resource/', function(req, res) {
	res.json(req.data);
});

router.get('/:resource?:type/', function(req, res) {
	res.json(req.data);
});

router.get('/:resource/:keys', async function(req, res) {
	res.json(req.data);
});

router.get('/:resource/:keys/:key',  async function(req, res) {
	res.json(req.data);
});

router.get('/:resource/:keys/:key/:last',  async function(req, res) {
	res.json(req.data);
});


router.use(function (err, req, res, next) {
  console.error(err.stack);
	res.status(404).send(err.message);
});

app.use('/', router);
var server = app.listen(3000,function() {} );